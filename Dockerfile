FROM openjdk:8
COPY authentication-server/target/authentication-server-1.0-SNAPSHOT.jar /home/authentication-server-1.0-SNAPSHOT.jar
CMD ["java","-jar", "/home/authentication-server-1.0-SNAPSHOT.jar"]
