package eu.innotex.authserver.services;

import eu.innotex.authserver.model.PasswordResetToken;
import eu.innotex.authserver.model.User;
import eu.innotex.authserver.model.UserRole;
import eu.innotex.authserver.repositories.PasswordResetTokenRepository;
import eu.innotex.authserver.repositories.UserRepository;
import eu.innotex.authserver.repositories.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class UserAuthService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private PasswordResetTokenRepository passwordResetTokenRepository;

    private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(4);


    //Queries

    public User getUserById(UUID uuid){
        return userRepository.getOne(uuid);
    }

    public User getUserByUsername(String username) {
        return userRepository.findUserByUsername(username);
    }

    public List<User> getAllUsers(){
        return userRepository.findAll();
    }

    public List<User> getRoleUsers(UserRole userRole){
        return userRepository.findAllByUserRoles(userRole);
    }

    public UserRole getRoleById(Long roleId){
        return userRoleRepository.getOne(roleId);
    }

    public List<UserRole> getAllRoles(){
        return userRoleRepository.findAll();
    }

    public List<UserRole> getRoleByName(String name){
        return userRoleRepository.findAllByNameContainsIgnoreCase(name);
    }



    //Commands

    public void createPasswordResetTokenForUser(User user, String token){

        PasswordResetToken resetToken = new PasswordResetToken();

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date()); // sets calendar time/date
        cal.add(Calendar.HOUR_OF_DAY, 24); // adds one hour

        resetToken.setToken(token);
        resetToken.setUser(user);
        resetToken.setExpiryDate(cal.getTime());

        passwordResetTokenRepository.save(resetToken);
    }

    public void changeUserPassword(User user, String password) {
        user.setPassword(passwordEncoder.encode(password));
        userRepository.save(user);
    }

    public User createNewUser(User user){

        return userRepository.save(user);
    }


}
