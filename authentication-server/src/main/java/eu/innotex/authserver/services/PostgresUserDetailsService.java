package eu.innotex.authserver.services;

import eu.innotex.authserver.model.User;
import eu.innotex.authserver.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class PostgresUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findUserByUsername(username);

        if(user==null){
            throw new UsernameNotFoundException("User "+username+" not found");
        }

        if (user.isAccountExpired()){
            throw new AccountExpiredException("Account Expired");
        }

        if (user.isAccountLocked()){
            throw new LockedException("Account Locked");
        }

        return user;

    }
}
