package eu.innotex.authserver.controller;

import eu.innotex.authserver.model.PasswordResetToken;
import eu.innotex.authserver.model.User;
import eu.innotex.authserver.model.UserRole;
import eu.innotex.authserver.payload.ResetPasswordDto;
import eu.innotex.authserver.payload.UpdatePasswordDto;
import eu.innotex.authserver.repositories.PasswordResetTokenRepository;
import eu.innotex.authserver.repositories.UserRepository;
import eu.innotex.authserver.services.UserAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
public class AuthServerController {

    @Autowired
    private UserAuthService userAuthService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordResetTokenRepository passwordResetTokenRepository;

    //Validate Logged-in User
    @RequestMapping(value = { "/user" }, produces = "application/json")
    public Map<String, Object> user(OAuth2Authentication user) {
        Map<String, Object> userInfo = new HashMap<>();
        userInfo.put("user", user.getUserAuthentication().getPrincipal());
        userInfo.put("authorities", AuthorityUtils.authorityListToSet(user.getUserAuthentication().getAuthorities()));
        return userInfo;
    }

    // Queries

    @GetMapping(value = "/user/id/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public User getUserById(@PathVariable UUID id){
        return userAuthService.getUserById(id);
    }

    @GetMapping(value = "/user/username/{username}", produces = MediaType.APPLICATION_JSON_VALUE)
    public User getUserByUsername(@PathVariable String username){
        return userAuthService.getUserByUsername(username);
    }

    @GetMapping(value = "/user/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<User> getAllUsers(){
        return userAuthService.getAllUsers();
    }

    @GetMapping(value = "/user/role/{roleId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<User> getRoleUsers(@PathVariable Long roleId) {
        UserRole userRole = userAuthService.getRoleById(roleId);
        return userAuthService.getRoleUsers(userRole);
    }

    @GetMapping(value = "/role/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<UserRole> getAllRoles(){
        return userAuthService.getAllRoles();
    }

    @GetMapping(value = "/role/id/{rid}", produces = MediaType.APPLICATION_JSON_VALUE)
    public UserRole getRoleById(@PathVariable Long roleId){
        return userAuthService.getRoleById(roleId);
    }

    @GetMapping(value = "/role/name/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<UserRole> getRolesByName(@PathVariable String name){
        return userAuthService.getRoleByName(name);
    }

    @GetMapping(value = "/validate/user/email/{userEmail}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity checkIfUserExists(@PathVariable String userEmail){
        String regex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@((?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6})$";
        Pattern pattern = Pattern.compile(regex);

        Matcher matcher = pattern.matcher(userEmail);
        if (!matcher.matches()){
            Map<String, Object> errorMsg = new HashMap<String, Object>();
            errorMsg.put("error", "Invalid email address: "+userEmail);
            errorMsg.put("datetime", new Date());
            return new ResponseEntity(errorMsg, HttpStatus.BAD_REQUEST);
        }

        Optional<User> user = Optional.ofNullable(userRepository.findUserByUsername(userEmail));

        if(user.isPresent()){
            Map<String, Object> errorMsg = new HashMap<String, Object>();
            errorMsg.put("error", "User already exists.");
            errorMsg.put("datetime", new Date());
            return new ResponseEntity(errorMsg, HttpStatus.BAD_REQUEST);
        }

        Map<String, Object> errorMsg = new HashMap<String, Object>();
        errorMsg.put("created", "ok");
        errorMsg.put("datetime", new Date());

        return new ResponseEntity(errorMsg, HttpStatus.OK);
    }

    @GetMapping(value = "/validate/organization/domain/{userEmail}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity checkOrganizationFromUserEmail(@PathVariable String userEmail){
        String regex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@((?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6})$";
        Pattern pattern = Pattern.compile(regex);

        Matcher matcher = pattern.matcher(userEmail);
        if (!matcher.matches()){
            Map<String, Object> errorMsg = new HashMap<String, Object>();
            errorMsg.put("error", "Invalid email address");
            errorMsg.put("datetime", new Date());
            return new ResponseEntity(errorMsg, HttpStatus.BAD_REQUEST);
        }

        List<User> users = userRepository.findAllByUsernameEndsWith(matcher.group(1));
        if (users.isEmpty()){
            Map<String, Object> errorMsg = new HashMap<String, Object>();
            errorMsg.put("error", "Email address does not match a registered organization. PLease contact us at eoc@ucy.ac.cy");
            errorMsg.put("datetime", new Date());
            return new ResponseEntity(errorMsg, HttpStatus.BAD_REQUEST);
        }

        //Get organization
//

        return null;
    }

    @GetMapping(value = "/validate/password/token/{passwordtoken}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity validatePasswordResetToken(@PathVariable String passwordtoken){

        PasswordResetToken resetToken = passwordResetTokenRepository.findPasswordResetTokenByToken(passwordtoken);

        if(resetToken == null){
            Map<String, String> errorMsg = new HashMap<String, String>();
            errorMsg.put("error", "Reset Token not found");
            return new ResponseEntity(errorMsg, HttpStatus.NOT_FOUND);
        }

        Calendar cal = Calendar.getInstance();
        if ((resetToken.getExpiryDate()
                .getTime() - cal.getTime()
                .getTime()) <= 0) {

            Map<String, String> errorMsg = new HashMap<String, String>();
            errorMsg.put("error", "Reset Token expired ");
            return new ResponseEntity(HttpStatus.BAD_REQUEST);

        }

        User user = resetToken.getUser();

        Authentication auth = new UsernamePasswordAuthenticationToken(
                user, null, Arrays.asList(
                new SimpleGrantedAuthority("CHANGE_PASSWORD_PRIVILEGE")));
        SecurityContextHolder.getContext().setAuthentication(auth);


        Map<String, String> errorMsg = new HashMap<String, String>();
        errorMsg.put("msg", "Valid password reset token for user "+user.getUsername());
        return new ResponseEntity(errorMsg, HttpStatus.OK);
    }

    @GetMapping(value = "/activate/user/{uid}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity activateUser(@PathVariable UUID uid){
        User user = userRepository.getOne(uid);

        if(user==null){
            Map<String, Object> errorMsg = new HashMap<String, Object>();
            errorMsg.put("error", "Invalid activation URL");
            errorMsg.put("datetime", new Date());
            return new ResponseEntity(errorMsg, HttpStatus.NOT_FOUND);
        }

        if(user.isUserActivated()){
            Map<String, Object> errorMsg = new HashMap<String, Object>();
            errorMsg.put("error", "User is already activated");
            errorMsg.put("datetime", new Date());
            return new ResponseEntity(errorMsg, HttpStatus.BAD_REQUEST);
        }

        user.setUserActivated(true);

        User u = userRepository.save(user);
        Map<String, Object> errorMsg = new HashMap<String, Object>();
        errorMsg.put("msg", "User activated");
        errorMsg.put("datetime", new Date());
        return new ResponseEntity(errorMsg, HttpStatus.OK);


    }

    // Commands

    //Reset Password
    @PostMapping(value = "/resetPassword", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity resetPassword(@RequestBody ResetPasswordDto resetPasswordDto){
        User user = userRepository.findUserByUsername(resetPasswordDto.getEmail());
        if (user==null){
            Map<String, String> errorMsg = new HashMap<String, String>();
            errorMsg.put("error", "Email not found!");
            return new ResponseEntity(errorMsg, HttpStatus.NOT_FOUND);
        }

        String passwordToken = UUID.randomUUID().toString();

        userAuthService.createPasswordResetTokenForUser(user, passwordToken);
        //TODO Send email to user with password reset link

        Map<String, String> msg = new HashMap<String, String>();
        msg.put("message", "Password reset email has been sent to "+user.getUsername());
        return new ResponseEntity(msg, HttpStatus.OK);
    }


    @PostMapping(value = "/user/saveNewPassword/uid/{id}/token/{passtoken}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity saveNewResetPassword(@RequestBody UpdatePasswordDto updatePasswordDto, @PathVariable UUID id, @PathVariable String passtoken){
        PasswordResetToken resetToken = passwordResetTokenRepository.findPasswordResetTokenByToken(passtoken);

        if(resetToken == null){
            Map<String, String> errorMsg = new HashMap<String, String>();
            errorMsg.put("error", "Reset Token not found!");
            return new ResponseEntity(errorMsg, HttpStatus.NOT_FOUND);
        }

        Calendar cal = Calendar.getInstance();
        if ((resetToken.getExpiryDate()
                .getTime() - cal.getTime()
                .getTime()) <= 0) {

            Map<String, String> errorMsg = new HashMap<String, String>();
            errorMsg.put("error", "Reset Token expired");
            return new ResponseEntity(errorMsg, HttpStatus.BAD_REQUEST);

        }

        User user = resetToken.getUser();

        User user_from_id = userRepository.getOne(id);

        if (user!= user_from_id){
            Map<String, String> errorMsg = new HashMap<String, String>();
            errorMsg.put("error", "You do not have the rights to perform this action");
            return new ResponseEntity(errorMsg, HttpStatus.BAD_REQUEST);
        }

        userAuthService.changeUserPassword(user, updatePasswordDto.getNewPassword());
        Map<String, String> errorMsg = new HashMap<String, String>();
        errorMsg.put("msg", "Password has been successfully updaed. Please login again.");
        return new ResponseEntity(errorMsg, HttpStatus.OK);

    }


    @PostMapping(value = "/register/user", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity registerUser(@RequestBody User user){
        User u = userAuthService.createNewUser(user);
        //TODO send email with activation url


        Map<String, String> errorMsg = new HashMap<String, String>();
        errorMsg.put("msg", "User has with id "+u.getUserId()+" has been successfully created");
        return new ResponseEntity(errorMsg, HttpStatus.OK);

    }

    @PostMapping(value = "/update/user/password", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity updateUser(@RequestBody UpdatePasswordDto passwordDto){
        User u = userRepository.findUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        userAuthService.changeUserPassword(u, passwordDto.getNewPassword());
        return new ResponseEntity<>(HttpStatus.OK);

    }




}
