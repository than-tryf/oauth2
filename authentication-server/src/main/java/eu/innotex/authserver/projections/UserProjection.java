package eu.innotex.authserver.projections;

import eu.innotex.authserver.model.User;
import eu.innotex.authserver.model.UserRole;
import org.springframework.data.rest.core.config.Projection;

import java.util.List;
import java.util.UUID;

@Projection(types = {User.class}, name = "userProjection")
public interface UserProjection {
    UUID getUserId();
    String getUsername();
    List<UserRole> getUserRoles();

}
