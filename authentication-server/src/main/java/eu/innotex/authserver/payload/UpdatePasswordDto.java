package eu.innotex.authserver.payload;

import java.io.Serializable;

public class UpdatePasswordDto implements Serializable {

    private String oldPassword;

    private String newPassword;


    public UpdatePasswordDto() {
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
