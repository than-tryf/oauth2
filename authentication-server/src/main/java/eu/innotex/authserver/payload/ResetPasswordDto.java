package eu.innotex.authserver.payload;

import java.io.Serializable;

public class ResetPasswordDto implements Serializable {

    private String email;

    public ResetPasswordDto() {
    }

    public ResetPasswordDto(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
