package eu.innotex.authserver.repositories;

import eu.innotex.authserver.model.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRoleRepository extends CrudRepository<UserRole, Long>, PagingAndSortingRepository<UserRole, Long>, JpaRepository<UserRole, Long> {
    List<UserRole> findAllByNameContainsIgnoreCase(String name);
}
