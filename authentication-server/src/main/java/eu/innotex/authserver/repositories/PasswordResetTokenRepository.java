package eu.innotex.authserver.repositories;

import eu.innotex.authserver.model.PasswordResetToken;
import eu.innotex.authserver.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface PasswordResetTokenRepository extends CrudRepository<PasswordResetToken, Long> {
    List<PasswordResetToken> findAllByUserAndExpiryDateBefore(User u, Date d);
    PasswordResetToken findPasswordResetTokenByToken(String resetToken);
}
