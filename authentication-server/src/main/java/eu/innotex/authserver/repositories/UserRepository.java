package eu.innotex.authserver.repositories;

import eu.innotex.authserver.model.User;
import eu.innotex.authserver.model.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserRepository extends CrudRepository<User, UUID>, PagingAndSortingRepository<User, UUID>, JpaRepository<User, UUID> {
    User findUserByUsername(@Param("username") String username);
    List<User> findAllByUserRoles(UserRole userRole);

    List<User> findAllByUsernameEndsWith(String domain);
}

