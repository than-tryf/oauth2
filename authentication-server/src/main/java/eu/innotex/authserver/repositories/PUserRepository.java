package eu.innotex.authserver.repositories;

import eu.innotex.authserver.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
@RepositoryRestResource(path = "pusers")
@PostAuthorize("hasRole('USER')")
public interface PUserRepository extends CrudRepository<User, UUID>, PagingAndSortingRepository<User, UUID>, JpaRepository<User, UUID> {
    User findUserByUsername(@Param("username") String username);
}
